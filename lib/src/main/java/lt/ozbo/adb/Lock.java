package lt.ozbo.adb;

import java.util.concurrent.Semaphore;

class Lock {
    private final Semaphore lock = new Semaphore(0);
    void unlock() {
        lock.release();
    }

    void lock() {
        try {
            lock.acquire(1);
        }
        catch (InterruptedException e) {
            // pass
        }
    }

    void await() {
        lock();
        unlock();
    }
}
