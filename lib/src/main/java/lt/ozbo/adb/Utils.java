package lt.ozbo.adb;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.cgutman.adblib.AdbBase64;
import com.cgutman.adblib.AdbCrypto;

class Utils {
	static final String PUBLIC_KEY_NAME = "public.key";
	static final String PRIVATE_KEY_NAME = "private.key";
	
	static AdbCrypto readCryptoConfig(File dataDir) {
		File pubKey = new File(dataDir, PUBLIC_KEY_NAME);
		File privKey = new File(dataDir, PRIVATE_KEY_NAME);

		if (pubKey.exists() && privKey.exists())
		{
			try {
				return AdbCrypto.loadAdbKeyPair(new AndroidBase64(), privKey, pubKey);
			}
			catch (Exception e) {
				return null;
			}
		}
		else {
			return writeNewCryptoConfig(dataDir);
		}
	}
	
	static AdbCrypto writeNewCryptoConfig(File dataDir) {
		File pubKey = new File(dataDir, PUBLIC_KEY_NAME);
		File privKey = new File(dataDir, PRIVATE_KEY_NAME);

		try {
			AdbCrypto crypto = AdbCrypto.generateAdbKeyPair(new AndroidBase64());
			crypto.saveAdbKeyPair(privKey, pubKey);
			return crypto;
		}
		catch (Exception e) {
			return null;
		}
	}
	
	static boolean safeClose(Closeable c) {
		if (c == null)
			return false;
		
		try {
			c.close();
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}

	static boolean deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			File[] files = fileOrDirectory.listFiles();
			if(files == null) {
				return false;
			}

			for (File child : files) {
				if(!deleteRecursive(child)) {
					return false;
				}
			}
		}

		return fileOrDirectory.delete();
	}

	static class FileIO {
		private final File path;

		FileIO(String filename) {
			this.path = new File(filename);
		}

		byte[] readAll() throws IOException {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			InputStream inputStream = new FileInputStream(path);
			int byteRead;

			while ((byteRead = inputStream.read()) != -1) {
				outputStream.write(byteRead);
			}

			inputStream.close();
			return outputStream.toByteArray();
		}

		void overwrite(byte[] data) throws IOException {
			OutputStream outputStream = new FileOutputStream(path);
			outputStream.write(data);
			outputStream.close();
		}
	}

	static class AndroidBase64 implements AdbBase64 {
		@Override
		public String encodeToString(byte[] data) {
			return Base64.encodeToString(data, Base64.NO_WRAP);
		}
	}
}
