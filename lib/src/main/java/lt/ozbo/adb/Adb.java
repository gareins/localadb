package lt.ozbo.adb;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class Adb implements AdbOutputListener, AdbStatusListener {
    public interface AsyncCommandResponse {
        void onResult(AdbResponse response);
    }

    private final Connection deviceConnection;
    private final AdbStatusListener listener;
    int parseStatus = 0;
    int returnValue = 0;
    private AsyncCommandResponse callback;
    private final Lock lock = new Lock();
    private boolean connected = false;
    private final String commandsFolder;

    public Adb(Context context, String host, int port, AdbStatusListener listener) {
        this.listener = listener;
        deviceConnection = new Connection(this, this, host, port);
        commandsFolder = context.getExternalFilesDir(null) + "/.adb/";

        File directory = new File(commandsFolder);
        if(directory.isDirectory() || directory.isFile()) {
            if (!Utils.deleteRecursive(directory)) {
                listener.notifyDisconnected(new AdbException("Unable to clean .adb directory"));
                return;
            }
        }

        if(!directory.mkdir()) {
            listener.notifyDisconnected(new AdbException("Unable to create .adb directory"));
            return;
        }

        deviceConnection.startConnect(context);
    }

    private String commandLocation(String path, boolean forAdb) {
        return (forAdb ? commandsFolder.replace("/storage/emulated/0", "/sdcard") : commandsFolder) + path;
    }

    private String commandsFile(boolean forAdb) {
        return commandLocation("commands", forAdb);
    }

    private String commandStdout(boolean forAdb) {
        return commandLocation("stdout", forAdb);
    }

    private String commandStderr(boolean forAdb) {
        return commandLocation("stderr", forAdb);
    }

    private void runCommandAsyncNoLock(String command, AsyncCommandResponse callback) throws IOException {
        new Utils.FileIO(commandsFile(false)).overwrite(command.getBytes(StandardCharsets.UTF_8));
        long milis = System.currentTimeMillis();
        String stderrTempFile = commandLocation("temp.stderr." + milis, true);
        String stdoutTempFile = commandLocation("temp.stdout." + milis, true);

        Log.d("CMD", command);
        String fullCommand = String.format("touch %1$s; touch %2$s; sh %5$s 1>%1$s 2>%2$s; E=$?; mv %1$s %3$s; mv %2$s %4$s; echo \"!!!!!$E!\"\n",
                stderrTempFile, stdoutTempFile, commandStdout(true), commandStderr(true), commandsFile(true));

        deviceConnection.queueCommand(fullCommand);
        this.callback = callback;
    }

    public void runCommandAsync(String command, AsyncCommandResponse callback) throws AdbException {
        lock.lock();

        try {
            runCommandAsyncNoLock(command, callback);
        }
        catch (IOException e) {
            throw new AdbException(e);
        }
    }

    @Override
    public void onData(byte[] data) {
        for(byte d: data) {
            if(parseStatus < 5 && d == '!') {
                parseStatus += 1;
            }
            else if(parseStatus == 5 && d <= '9' && d >= '0') {
                returnValue = returnValue * 10 + (d - '0');
            }
            else if(parseStatus == 5 && d == '!') {
                onCommandFinished();
                parseStatus = 0;
                returnValue = 0;
            }
            else {
                parseStatus = 0;
                returnValue = 0;
            }
        }
    }

    private void onCommandFinished() {
        AdbResponse response = new AdbResponse();
        response.status = returnValue;
        try {
            response.stdout = new Utils.FileIO(commandStdout(false)).readAll();
            response.stderr = new Utils.FileIO(commandStderr(false)).readAll();
        }
        catch (IOException e) {
            throw new Error(e);
        }

        lock.unlock();
        if(callback != null) {
            callback.onResult(response);
        }
    }

    public AdbResponse runCommandSync(String command) throws AdbException {
        long startTime = System.currentTimeMillis();

        AdbResponse response = new AdbResponse();
        runCommandAsync(command, (r) -> {
            response.stdout = r.stdout;
            response.status = r.status;
            response.stderr = r.stderr;
        });

        lock.await();
        Log.d("Finished: ", "" + response.stdout + " :: " + response.stderr + " :: " + response.status + ", time: " + (System.currentTimeMillis() - startTime));
        return response;
    }

    public void checkCommandSync(String command) throws AdbException {
        AdbResponse response = runCommandSync(command);

        if(response.status != 0) {
            throw new AdbException("Command exited with non zero code: " + response.status);
        }
    }

    @Override
    public void notifyConnected() {
        connected = true;
        listener.notifyConnected();
        lock.unlock();
    }

    @Override
    public void notifyDisconnected(AdbException reason) {
        connected = false;
        listener.notifyDisconnected(reason);

        if(Objects.equals(reason.getMessage(), AdbStatusListener.StreamClosed)) {
            reason.printStackTrace();
        }
    }

    public void close() {
        deviceConnection.close();
    }

    public boolean isConnected() {
        return connected;
    }
}
