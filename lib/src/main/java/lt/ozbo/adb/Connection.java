package lt.ozbo.adb;

import android.content.Context;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.LinkedBlockingQueue;

import com.cgutman.adblib.AdbConnection;
import com.cgutman.adblib.AdbCrypto;
import com.cgutman.adblib.AdbStream;

class Connection implements Closeable {
	private static final int CONN_TIMEOUT = 1000;

	private final String host;
	private final int port;
	private final AdbStatusListener listener;
	private final AdbOutputListener outputListener;

	private AdbConnection connection;
	private AdbStream shellStream;
	
	private boolean closed;
	private final LinkedBlockingQueue<byte[]> commandQueue = new LinkedBlockingQueue<>();
	
	public Connection(AdbStatusListener listener, AdbOutputListener outputListener, String host, int port) {
		this.host = host;
		this.port = port;
		this.listener = listener;
		this.outputListener = outputListener;
	}
	
	public void queueCommand(String command) {
		commandQueue.add(command.getBytes(StandardCharsets.UTF_8));
	}

	public void startConnect(Context context) {
		new Thread(() -> {
			boolean connected = false;
			Socket socket = new Socket();
			AdbCrypto crypto;

			/* Load the crypto config */
			crypto = Utils.readCryptoConfig(context.getFilesDir());
			if (crypto == null) {
				return;
			}

			try {
				/* Establish a connect to the remote host */
				socket.connect(new InetSocketAddress(host, port), CONN_TIMEOUT);
			}
			catch (IOException e) {
				listener.notifyDisconnected(new AdbException(e));
				return;
			}

			try {
				/* Establish the application layer connection */
				connection = AdbConnection.create(socket, crypto);
				connection.connect();

				/* Open the shell stream */
				shellStream = connection.open("shell:");
				connected = true;
			}
			catch (IOException | InterruptedException e) {
				listener.notifyDisconnected(new AdbException(e));
			}
			finally {
				/* Cleanup if the connection failed */
				if (!connected) {
					Utils.safeClose(shellStream);

					/* The AdbConnection object will close the underlying socket
					 * but we need to close it ourselves if the AdbConnection object
					 * wasn't successfully constructed.
					 */
					if (!Utils.safeClose(connection)) {
						try {
							socket.close();
						}
						catch (IOException ignored) {}
					}
				}
			}

			if(connected) {
				/* Notify the listener that the connection is complete */
				listener.notifyConnected();

				/* Start the receive thread */
				startReceiveThread();

				/* Enter the blocking send loop */
				sendLoop();
			}
		}).start();
	}
	
	private void sendLoop() {
		/* We become the send thread */
		try {
			for (;;) {
				/* Get the next command */
				byte[] command = commandQueue.take();
				
				/* This may be a close indication */
				if (shellStream.isClosed()) {
					listener.notifyDisconnected(new AdbException(AdbStatusListener.StreamClosed));
					break;
				}
				
				/* Issue it to the device */
				shellStream.write(command);
			}
		}
		catch (IOException | InterruptedException e) {
			listener.notifyDisconnected(new AdbException(e));
		}
		finally {
			Utils.safeClose(Connection.this);
		}
	}
	
	private void startReceiveThread() {
		new Thread(() -> {
			try {
				while (!shellStream.isClosed()) {
					outputListener.onData(shellStream.read());
				}
				listener.notifyDisconnected(new AdbException(AdbStatusListener.StreamClosed));
			}
			catch (IOException | InterruptedException e) {
				listener.notifyDisconnected(new AdbException(e));
			}
			finally {
				Utils.safeClose(Connection.this);
			}
		}).start();
	}
	
	public boolean isClosed() {
		return closed;
	}

	@Override
	public void close() {
		if (isClosed()) {
			return;
		}
		else {
			closed = true;
		}
		
		/* Close the stream first */
		Utils.safeClose(shellStream);
		
		/* Now the connection (and underlying socket) */
		Utils.safeClose(connection);
		
		/* Finally signal the command queue to allow the send thread to terminate */
		commandQueue.add(new byte[0]);
	}
}
