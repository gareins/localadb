package lt.ozbo.adb;

public class AdbResponse {
    public byte[] stdout;
    public byte[] stderr;
    public int status;
}
