package lt.ozbo.adb;

public interface AdbStatusListener {
    String StreamClosed = "Stream closed";

    void notifyConnected();
    void notifyDisconnected(AdbException reason);
}