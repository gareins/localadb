package lt.ozbo.adb;

public class AdbException extends Exception {
    public AdbException(Exception e) {
        super(e);
    }

    public AdbException(String msg) {
        super(msg);
    }
}
