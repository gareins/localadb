package lt.ozbo.adb;

public interface AdbOutputListener {
    void onData(byte[] data);
}
